import torchvision
from torchvision.transforms import InterpolationMode, Resize


class ResizeLongestSideDivisible():
    def __init__(self, img_size, divider):
        self.divider = divider
        self._op = None
        self._op_target = None

        # Transform input to DINO format:
        #  - can have arbitrary size, but it must be divisible by 14 (patch size of the used VIT backbone)
        #  - keeps aspect ratio, no padding needed
        if isinstance(img_size, list) and len(img_size) == 2:
            self.img_sz = img_size
            if self.img_sz[0] % self.divider > 0 or self.img_sz[1] % self.divider > 0:
                raise RuntimeError(f"INPUT.IMG_SIZE has to be divisible by 14")
            self._op = Resize(size=self.img_sz)
            self._op_target = Resize(size=self.img_sz, interpolation=InterpolationMode.NEAREST)
        elif isinstance(img_size, int) and img_size % self.divider == 0:
            # longest side stored in IMG_SIZE
            self.img_sz = img_size
        else:
            raise RuntimeError(f"INPUT.IMG_SIZE has to be list[2] or int and divisible by {divider}!")

    def __call__(self, image):
        if self._op is not None:
            image = self._op(image)
            return image
        else:
            # assuming image is tensor of [..., h, w] shape
            x_size = image.shape[-2:]

            if x_size[0] >= x_size[1]:
                factor = x_size[0] / float(self.img_sz)
                size = [int(self.img_sz), int(self.divider*((x_size[1] / factor) // self.divider))] 
            else:
                factor = x_size[1] / float(self.img_sz)
                size = [int(self.divider*((x_size[0] / factor) // self.divider)), int(self.img_sz)] 

            image = torchvision.transforms.functional.resize(image, size, antialias=True)

            return image
