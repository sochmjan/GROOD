import os
from torchvision import datasets


class SimpleImageFolder():
    def __init__(self, cfg, dataset_root, transform_train=None, transform_test=None):
        self.dataset_root = dataset_root
        self.transform_train = transform_train
        self.transform_test = transform_test

        selected_labels = cfg.DATASET.SELECTED_LABELS
        self.class_map = dict(zip(selected_labels, range(len(selected_labels))))
        self.inverse_class_map = dict(zip(range(len(selected_labels)), selected_labels))

    def get_split(self, split):
        if split == "train":
            return datasets.ImageFolder(os.path.join(self.dataset_root, "train"), transform=self.transform_train)
        elif split == "val":
            return datasets.ImageFolder(os.path.join(self.dataset_root, "val"), transform=self.transform_test)
        elif split == "test": 
            return datasets.ImageFolder(os.path.join(self.dataset_root, "test"), transform=self.transform_test)
        else:
            raise NotImplementedError
