from torchvision.transforms import ToTensor, AutoAugment, Compose, RandomCrop, RandomHorizontalFlip, Resize, CenterCrop, Normalize
from torchvision.transforms import InterpolationMode
import torch
from dataloaders.transforms import ResizeLongestSideDivisible


class NoAugmentation():
    def train(self, cfg):
        augment = Compose([
            Resize((cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            ToTensor()
        ])

        return augment
    
    def test(self, cfg):
        augment = Compose([
            Resize((cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)), 
            ToTensor()
        ])

        return augment


class BasicAugmentation():
    def train(self, cfg):
        augment = Compose([
            Resize((cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),  
            RandomCrop(cfg.INPUT.IMG_SZ, padding=4), 
            RandomHorizontalFlip(), 
            ToTensor()
        ])

        return augment
    
    def test(self, cfg):
        augment = Compose([
            Resize((cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)), 
            ToTensor()
        ])

        return augment


class AutoAugmentation():
    def train(self, cfg):
        augment = Compose([
            Resize((cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)), 
            AutoAugment(),
            ToTensor()
        ])

        return augment
    
    def test(self, cfg):
        augment = Compose([
            Resize((cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)), 
            ToTensor()
        ])

        return augment


class CLIPAugmentation():
    def train(self, cfg):
        augment = Compose([
            Resize(size=cfg.INPUT.IMG_SZ, interpolation=InterpolationMode.BICUBIC, max_size=None, antialias=None),
            CenterCrop(size=(cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            ToTensor(),
            Normalize(mean=(0.48145466, 0.4578275, 0.40821073), std=(0.26862954, 0.26130258, 0.27577711))
        ])

        return augment
    
    def test(self, cfg):
        augment = Compose([
            Resize(size=cfg.INPUT.IMG_SZ, interpolation=InterpolationMode.BICUBIC, max_size=None, antialias=None),
            CenterCrop(size=(cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            ToTensor(),
            Normalize(mean=(0.48145466, 0.4578275, 0.40821073), std=(0.26862954, 0.26130258, 0.27577711))
        ])

        return augment


class ResNetImageNetAugmentation():
    def train(self, cfg):
        augment = Compose([
            Resize(224),
            CenterCrop(size=(cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            ToTensor(),
            Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])
        
        return augment
    
    def test(self, cfg):
        augment = Compose([
            Resize(224),
            CenterCrop(size=(cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            ToTensor(),
            Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
        ])

        return augment


class AddLines(object):
    """ Adds a single red horizontal line to every image
    """
    def __init__(self):
        return
    
    def __call__(self, sample):
        # print(f"sample: {sample}")
        # assert(len(sample.shape) == 3)

        # row_idx = torch.randint(0, sample.shape[1]-3, (1,))
        pixels = sample.load()
        row_idx = torch.randint(0, sample.height-3, (1,))
        for c in range(sample.width):
           pixels[c, row_idx] = (255, 0, 0) 
           pixels[c, row_idx+1] = (255, 0, 0) 
           pixels[c, row_idx+2] = (255, 0, 0) 
        # sample[0, row_idx:row_idx+2, :] = 1.0
        # sample[1, row_idx:row_idx+2, :] = 0.0
        # sample[2, row_idx:row_idx+2, :] = 0.0

        return sample


class CLIPAugmentation_lines():
    def train(self, cfg):
        augment = Compose([
            Resize(size=cfg.INPUT.IMG_SZ, interpolation=InterpolationMode.BICUBIC, max_size=None, antialias=None),
            CenterCrop(size=(cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            AddLines(),
            ToTensor(),
            Normalize(mean=(0.48145466, 0.4578275, 0.40821073), std=(0.26862954, 0.26130258, 0.27577711))
        ])

        return augment
    
    def test(self, cfg):
        augment = Compose([
            Resize(size=cfg.INPUT.IMG_SZ, interpolation=InterpolationMode.BICUBIC, max_size=None, antialias=None),
            CenterCrop(size=(cfg.INPUT.IMG_SZ, cfg.INPUT.IMG_SZ)),
            AddLines(),
            ToTensor(),
            Normalize(mean=(0.48145466, 0.4578275, 0.40821073), std=(0.26862954, 0.26130258, 0.27577711))
        ])

        return augment

class DINOv2Augmentation():
    # Transform input to DINOv2 format:
    #  - values are normalized by mean and std of imagenet (see IMAGENET_DEFAULT_{MEAN, STD} 
    #    from https://github.com/facebookresearch/dinov2/blob/main/dinov2/data/transforms.py) 
    #  - can have arbitrary size, but it must be divisible by 14 (patch size of the used VIT backbone)
    #  - keeps aspect ratio, no padding needed

    def train(self, cfg):
        augment = Compose([
            ToTensor(),
            ResizeLongestSideDivisible(cfg.INPUT.IMG_SZ, cfg.MODEL.PATCH_SIZE),
            Normalize(mean=cfg.INPUT.NORM_MEAN, std=cfg.INPUT.NORM_STD)
        ])
        return augment
    
    def test(self, cfg):
        augment = Compose([
            ToTensor(),
            ResizeLongestSideDivisible(cfg.INPUT.IMG_SZ, cfg.MODEL.PATCH_SIZE),
            Normalize(mean=cfg.INPUT.NORM_MEAN, std=cfg.INPUT.NORM_STD)
        ])
        return augment
