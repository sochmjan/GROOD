import numpy as np
import pickle
from imageio import imwrite
import os
import sys


def unpickle_batchfile(file):
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='latin1')
    return dict
    

cifar10_data_dir = "_data/cifar-10-batches-py"
out_dir = "_data/cifar10_2x2relational"

if len(sys.argv) < 4:
    print(f"Usage: {sys.argv[0]} <train|val|test> num_samples <ID|OOD>")
    exit(1)

# parse parameters
dataset_split = sys.argv[1]     # train / val / test
num_out_per_class = int(sys.argv[2])
data_type = sys.argv[3]

# load all batches
imgs = np.empty((0, 3072))
labels = []
if dataset_split == 'train' or dataset_split == 'val':
    for b in range(5):
        bdict = unpickle_batchfile(f"{cifar10_data_dir}/data_batch_{b+1}")
        labels += bdict['labels']
        imgs = np.concatenate((imgs, bdict['data']), axis=0)

else:
    bdict = unpickle_batchfile(f"{cifar10_data_dir}/test_batch")
    labels += bdict['labels']
    imgs = np.concatenate((imgs, bdict['data']), axis=0)

# post-process the images
imgs = np.reshape(imgs, (imgs.shape[0], 3, 32, 32))
imgs = np.transpose(imgs, (0, 2, 3, 1))

imin = imgs.min()
imax = imgs.max()
imgs = (imgs - imin) / (imax - imin)

N = len(labels)

class_idxs = []
for c in range(10):
    class_idxs.append([i for i in range(N) if labels[i] == c])

if data_type == 'ID':
    for out_class in range(4):
        for i in range(num_out_per_class):
            rnd_lbl1, rnd_lbl2 = np.random.permutation(10)[:2]
            idx1, idx2, idx3 = np.random.permutation(class_idxs[rnd_lbl1])[:3]
            idx4 = np.random.permutation(class_idxs[rnd_lbl2])[0]

            if out_class == 0:      # A A \\ A B
                idx1, idx2, idx3, idx4 = idx1, idx2, idx3, idx4
            elif out_class == 1:    # A A \\ B A
                idx1, idx2, idx3, idx4 = idx1, idx2, idx4, idx3
            elif out_class == 2:    # A B \\ A A
                idx1, idx2, idx3, idx4 = idx1, idx4, idx2, idx3
            elif out_class == 3:    # B A \\ A A
                idx1, idx2, idx3, idx4 = idx4, idx1, idx2, idx3
                
            out_img = np.vstack((np.hstack((imgs[idx1, ...], imgs[idx2, ...])), np.hstack((imgs[idx3, ...], imgs[idx4, ...]))))
            
            fname = os.path.join(out_dir, data_type, dataset_split, f"class{out_class}", f"{i:09}.png")
            os.makedirs(os.path.dirname(fname), exist_ok=True)
            # print(fname)
            imwrite(fname, np.round(out_img * 255).astype(np.uint8))
else:   # OOD
    for out_class in range(4):
        for i in range(num_out_per_class):
            rnd_lbl1, rnd_lbl2, rnd_lbl3, rnd_lbl4 = np.random.permutation(10)[:4]

            if out_class == 0:      # A B \\ B A
                idx1, idx2 = np.random.permutation(class_idxs[rnd_lbl1])[:2]
                idx3, idx4 = np.random.permutation(class_idxs[rnd_lbl2])[:2]
                idx1, idx2, idx3, idx4 = idx1, idx3, idx4, idx2
            elif out_class == 1:    # A A \\ B B or A B \\ A B
                idx1, idx2 = np.random.permutation(class_idxs[rnd_lbl1])[:2]
                idx3, idx4 = np.random.permutation(class_idxs[rnd_lbl2])[:2]
                if np.random.rand() < 0.5:
                    idx1, idx2, idx3, idx4 = idx1, idx3, idx2, idx4
            elif out_class == 2:    # A A \\ B C or a rotation of this
                idx1, idx2 = np.random.permutation(class_idxs[rnd_lbl1])[:2]
                idx3 = np.random.permutation(class_idxs[rnd_lbl2])[0]
                idx4 = np.random.permutation(class_idxs[rnd_lbl3])[0]
                r = np.random.rand()
                if r < 0.25:
                    idx1, idx2, idx3, idx4 = idx4, idx1, idx3, idx2
                elif r < 0.5:
                    idx1, idx2, idx3, idx4 = idx3, idx4, idx1, idx2 
                elif r < 0.75:
                    idx1, idx2, idx3, idx4 = idx1, idx3, idx2, idx4 
            elif out_class == 3:
                idx1 = np.random.permutation(class_idxs[rnd_lbl1])[0]
                idx2 = np.random.permutation(class_idxs[rnd_lbl2])[0]
                idx3 = np.random.permutation(class_idxs[rnd_lbl3])[0]
                idx4 = np.random.permutation(class_idxs[rnd_lbl4])[0]
                
            out_img = np.vstack((np.hstack((imgs[idx1, ...], imgs[idx2, ...])), np.hstack((imgs[idx3, ...], imgs[idx4, ...]))))
            
            fname = os.path.join(out_dir, data_type, dataset_split, f"class{out_class}", f"{i:09}.png")
            os.makedirs(os.path.dirname(fname), exist_ok=True)
            # print(fname)
            imwrite(fname, np.round(out_img * 255).astype(np.uint8))
    

        