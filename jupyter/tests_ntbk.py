#%% -------------------------------------------------------
# move one dir up from the jupyter directory
import os
os.chdir('..')


#%% -------------------------------------------------------
import numpy as np
import torch
import torch.nn.functional as F

import lovely_tensors as lt
lt.monkey_patch()
from lovely_numpy import lo

from einops import rearrange 
from types import SimpleNamespace
import pandas as pd

import matplotlib.pyplot as plt

from seaborn import kdeplot

import pickle

from grood import GROOD 
Experiment = GROOD

from utils import get_free_gpus, prepare_dataloaders, plot_train_test_errors, show_filters, reconstruct_image, reconstruct_prev_emb, compare_two_models

plt.style.use('default')

# %% --------------------------------------------------
# select a free GPU
free_gpus = get_free_gpus()
print('Free GPUs: {}'.format(free_gpus))
if len(free_gpus) == 0:
    print('NO FREE GPU!!!')
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = free_gpus[-1]
    print('GPU {} selected'.format(free_gpus[-1]))


# %% --------------------------------------------------
exp_name = 'red_line'
args = SimpleNamespace(exp_dir = './_out/experiments/' + exp_name,
                    #    latest_checkpoint = False)
                       latest_checkpoint = True)

# %% --------------------------------------------------
experiment = Experiment(exp_dir = args.exp_dir, eval_last_checkpoint=bool(args.latest_checkpoint))
cfg = experiment.cfg
# cfg.DATASET.TEST = 'cifar10lines'
# cfg.DATASET.TEST = 'svhn'

# cfg.merge_from_list(["DATASET.TEST_LABELS_OFFSET", 0])


# %% --------------------------------------------------
# Define dataloaders
test_loader_id, test_loader_ood = prepare_dataloaders(cfg)


# %% --------------------------------------------------
for (X, y) in test_loader_ood:
    break

# %% --------------------------------------------------
images = rearrange(X, "b c w h -> b w h c").detach().numpy()

idx = 0
img_min = images[idx, ...].min()
img_max = images[idx, ...].max()
plt.imshow((images[0, ...] - img_min) / (img_max - img_min))

# %% --------------------------------------------------
def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='latin1')
    return dict


# %% --------------------------------------------------
imgs = np.empty((0, 3072))
labels = []
for b in range(5):
    bdict = unpickle(f"_data/cifar-10-batches-py/data_batch_{b+1}")
    labels += bdict['labels']
    imgs = np.concatenate((imgs, bdict['data']), axis=0)

imgs = np.reshape(imgs, (imgs.shape[0], 3, 32, 32))
imgs = np.transpose(imgs, (0, 2, 3, 1))

imin = imgs.min()
imax = imgs.max()
imgs = (imgs - imin) / (imax - imin)


# %% --------------------------------------------------
idx = 2
img = imgs[idx, ...]

plt.imshow(img)


# %% --------------------------------------------------
N = len(labels)
M = 2

class_idxs = []
for c in range(10):
    class_idxs.append([i for i in range(N) if labels[i] == c])

for out_class in range(4):
    for i in range(M):
        rnd_lbl1, rnd_lbl2 = np.random.permutation(10)[:2]
        idx1, idx2, idx3 = np.random.permutation(class_idxs[rnd_lbl1])[:3]
        idx4 = np.random.permutation(class_idxs[rnd_lbl2])[0]

        if out_class == 0:
            idx1, idx2, idx3, idx4 = idx1, idx2, idx3, idx4
        elif out_class == 1:
            idx1, idx2, idx3, idx4 = idx1, idx2, idx4, idx3
        elif out_class == 2:
            idx1, idx2, idx3, idx4 = idx1, idx4, idx2, idx3
        elif out_class == 3:
            idx1, idx2, idx3, idx4 = idx4, idx1, idx2, idx3
            
        out_img = np.vstack((np.hstack((imgs[idx1, ...], imgs[idx2, ...])), np.hstack((imgs[idx3, ...], imgs[idx4, ...]))))

        plt.figure()
        plt.imshow(out_img)
        plt.title(f"class: {out_class}")


# %% --------------------------------------------------
