import nvsmi
import os
from dataloaders import make_datasets
from torch.utils.data import Subset
import torch
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import torchvision
from torchvision.utils import make_grid
import torchvision.transforms.functional as tvf
from mpl_toolkits.axes_grid1 import make_axes_locatable
from einops import rearrange 
from types import SimpleNamespace
from grood import GROOD 

Experiment = GROOD



def get_free_gpus(ignore_my_pid = True):
    used_gpu_ids = {p.gpu_id for p in nvsmi.get_gpu_processes() if not ignore_my_pid or p.pid != os.getpid()}

    free_gpus = [g.id for g in nvsmi.get_gpus() if g.id not in used_gpu_ids]

    return free_gpus


def prepare_dataloaders(cfg):
    # Define dataloaders
    train_set, val_set, test_set, inverse_class_map = make_datasets(cfg)
    targets = test_set.targets if hasattr(test_set, "targets") else test_set.labels
    if cfg.DATASET.TEST == cfg.DATASET.TRAIN:
        id_indices = [idx for idx, target in enumerate(targets) if target in cfg.DATASET.SELECTED_LABELS]
        ood_indices = [idx for idx, target in enumerate(targets) if target not in cfg.DATASET.SELECTED_LABELS]

        # Sanity checks
        id_mask = np.zeros(len(targets), dtype=int)
        id_mask[id_indices] = 1 
        ood_mask = np.zeros(len(targets), dtype=int)
        ood_mask[ood_indices] = 1 
        assert np.sum(id_mask*ood_mask) == 0, "Data selection error, there is an overlap between ID and OOD classes!"
        assert np.sum((id_mask+ood_mask) > 0) == len(targets), "Data selection error, not all data were selected!"

        test_loader_ood = torch.utils.data.DataLoader(Subset(test_set, ood_indices), batch_size=cfg.INPUT.BATCH_SIZE, shuffle=False)
        test_loader_id = torch.utils.data.DataLoader(Subset(test_set, id_indices), batch_size=cfg.INPUT.BATCH_SIZE, shuffle=False)
    else:
        test_set_sub = test_set
        if cfg.DATASET.OOD_SELECTED_LABELS is not None:
            ood_indices = [idx for idx, target in enumerate(targets) if target in cfg.DATASET.OOD_SELECTED_LABELS]
            test_set_sub = Subset(test_set, ood_indices)
        
        test_loader_ood = torch.utils.data.DataLoader(test_set_sub, batch_size=cfg.INPUT.BATCH_SIZE, shuffle=False)

        cfg_alt = cfg.clone()
        cfg_alt.defrost()
        cfg_alt.DATASET.TEST = cfg.DATASET.TRAIN
        _, _, test_set_id, _ = make_datasets(cfg_alt)

        id_targets = test_set_id.targets if hasattr(test_set_id, "targets") else test_set_id.labels
        id_indices = [idx for idx, target in enumerate(id_targets) if target in cfg_alt.DATASET.SELECTED_LABELS]
        test_loader_id = torch.utils.data.DataLoader(Subset(test_set_id, id_indices), batch_size=cfg.INPUT.BATCH_SIZE, shuffle=False)
        
    return test_loader_id, test_loader_ood


def plot_train_test_errors(args):
    train_loss = pd.read_csv(os.path.join(args.exp_dir, 'metrics', 'metric_train_loss.txt')).to_numpy()
    val_loss = pd.read_csv(os.path.join(args.exp_dir, 'metrics', 'metric_val_loss.txt')).to_numpy()

    plt.figure()
    plt.plot(train_loss[:, 0], train_loss[:, 1], label='train')
    plt.plot(val_loss[:, 0], val_loss[:, 1], label='val')
    plt.grid(True)
    plt.legend()
    plt.xlabel('epoch')
    plt.ylabel('loss')

    val_acc_file = os.path.join(args.exp_dir, 'metrics', 'metric_val_accuracy.txt')
    # old experiments didn't have this output
    if os.path.exists(val_acc_file):
        val_acc = pd.read_csv(val_acc_file).to_numpy()

        plt.figure()
        plt.plot(val_acc[:, 0], val_acc[:, 1], label='val')
        plt.grid(True)
        plt.legend()
        plt.xlabel('epoch')
        plt.ylabel('classification accuracy')


def show_filters(weights, layer, exp_name):
    """
        Shows the found weights.

        transpose: bools
            If to plot the grid transposed
    """
    w = weights[layer]
    w = w.cpu().detach().numpy()

    w_imgs = rearrange(w, "F C W H -> (F C) W H")
    res_op = torchvision.transforms.Resize((30, 30), tvf.InterpolationMode.NEAREST)
    w_imgs = w_imgs[:, None, :, :]
    w_imgs = res_op.forward(torch.tensor(w_imgs))
    # TODO: pad_value does not seem to be used...
    grid = make_grid(w_imgs, w.shape[1], pad_value=0.0) #, normalize=True, value_range=(-1.0, 1.0))
    grid = rearrange(grid, "C H W -> H W C").numpy()

    plt.figure(figsize=(30, 30))
    im = plt.imshow(grid, cmap='gray', vmin=-1, vmax=1)
    plt.rcParams.update({'font.size': 32})
    plt.title(f"exp: {exp_name}, layer: {layer}")
    plt.xlabel('channels')
    plt.ylabel('filters')
    plt.xticks([])
    plt.yticks([])
    
    # colorbar (same size as the image)
    divider = make_axes_locatable(plt.gca())
    cax = divider.append_axes("right", size="5%", pad=0.1)
    plt.colorbar(im, cax=cax)
    plt.rcParams.update({'font.size': 10})


def reconstruct_image(img_idx, emb, w, f_size=5, channels=3):
    """
    Reconstructs the input image from the given embeding.
    
    img_idx: int
        image index
    """
    H, W = [s + f_size - 1 for s in emb.shape[2:]]
    img_rec = np.zeros((H, W))
    counts = np.zeros((H, W))
    w_zeros = w.copy()
    w_zeros = (w_zeros + 1.0) / 2.0

    _, inds = emb[img_idx].topk(1, dim=0, sorted=True, largest=True)
    ind_H, ind_W = inds.squeeze().shape
    values = np.linspace(0.0, 1.0, channels)
    for r in np.arange(ind_H):
        for c in np.arange(ind_W):
            for i in np.arange(channels):
                img_rec[r:r+f_size, c:c+f_size] += w_zeros[inds[0, r, c], i, ...] * values[i]
                counts[r:r+f_size, c:c+f_size] += 1.0
            
    img_rec /= counts
    
    return img_rec


def reconstruct_prev_emb(img_idx, emb, w):
    """
    Reconstructs the previous layer embedding.
    
    img_idx: int
        image index
    """
    # w: [O, I, kH, kW]
    # emb: [B, O, Hcur, Wcur]
    # prev_emb: [B, I, H, W]
    channels = w.shape[1]
    f_size = w.shape[2]
    Hcur, Wcur = emb.shape[2:]
    H, W = [s + f_size - 1 for s in emb.shape[2:]]
    prev_emb = np.zeros((channels, H, W))
    counts = np.zeros((channels, H, W))
    w_zeros = w.copy()
    w_zeros = (w_zeros + 1.0) / 2.0

    _, inds = emb[img_idx].topk(1, dim=0, sorted=True, largest=True)
    # ind_H, ind_W = inds.squeeze().shape
    inds = inds.squeeze()
    # values = np.linspace(0.0, 1.0, channels)
    for r in np.arange(Hcur):
        for c in np.arange(Wcur):
            # for i in np.arange(channels):
            prev_emb[:, r:r+f_size, c:c+f_size] += w[inds[r, c], ...]
            counts[:, r:r+f_size, c:c+f_size] += 1.0
            
    prev_emb /= counts
    
    return prev_emb


def compare_two_models(exp_name1, exp_name2):
    args = SimpleNamespace(exp_dir = './_out/experiments/' + exp_name1,
                           latest_checkpoint = True)
    experiment1 = Experiment(exp_dir = args.exp_dir, eval_last_checkpoint=bool(args.latest_checkpoint))
    args = SimpleNamespace(exp_dir = './_out/experiments/' + exp_name2,
                           latest_checkpoint = True)
    experiment2 = Experiment(exp_dir = args.exp_dir, eval_last_checkpoint=bool(args.latest_checkpoint))
    
    model1 = experiment1.model
    model2 = experiment2.model
    
    diff = 0
    for p1, p2 in zip(model1.block1.parameters(), model2.block1.parameters()):
        diff += torch.abs(p1- p2).sum() > 0
    if diff > 0:
        print("Block 1: DIFFERENT")
    else:
        print("Block 1: SAME")

    diff = 0
    for p1, p2 in zip(model1.block2.parameters(), model2.block2.parameters()):
        diff += torch.abs(p1- p2).sum() > 0
    if diff > 0:
        print("Block 2: DIFFERENT")
    else:
        print("Block 2: SAME")

    diff = 0
    for p1, p2 in zip(model1.block3.parameters(), model2.block3.parameters()):
        diff += torch.abs(p1- p2).sum() > 0
    if diff > 0:
        print("Block 3: DIFFERENT")
    else:
        print("Block 3: SAME")

    diff = 0
    for p1, p2 in zip(model1.classifier1.parameters(), model2.classifier1.parameters()):
        diff += torch.abs(p1- p2).sum() > 0
    if diff > 0:
        print("Classifier 1: DIFFERENT")
    else:
        print("Classifier 1: SAME")

    diff = 0
    for p1, p2 in zip(model1.classifier2.parameters(), model2.classifier2.parameters()):
        diff += torch.abs(p1- p2).sum() > 0
    if diff > 0:
        print("Classifier 2: DIFFERENT")
    else:
        print("Classifier 2: SAME")
