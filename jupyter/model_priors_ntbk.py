#%% -------------------------------------------------------
# move one dir up from the jupyter directory
import os
os.chdir('..')


#%% -------------------------------------------------------
import numpy as np
import torch
import torch.nn.functional as F

import lovely_tensors as lt
lt.monkey_patch()
from lovely_numpy import lo

from einops import rearrange 
from types import SimpleNamespace
import pandas as pd

import matplotlib.pyplot as plt

from seaborn import kdeplot

import pickle

from grood import GROOD 
Experiment = GROOD

from utils import get_free_gpus, prepare_dataloaders, plot_train_test_errors, show_filters, reconstruct_image, reconstruct_prev_emb, compare_two_models

plt.style.use('default')

# %% --------------------------------------------------
# select a free GPU
free_gpus = get_free_gpus()
print('Free GPUs: {}'.format(free_gpus))
if len(free_gpus) == 0:
    print('NO FREE GPU!!!')
else:
    os.environ['CUDA_VISIBLE_DEVICES'] = free_gpus[-1]
    print('GPU {} selected'.format(free_gpus[-1]))

# %% --------------------------------------------------
exp_name = 'dinov2_c10_2x2rel'
args = SimpleNamespace(exp_dir = './_out/experiments/' + exp_name,
                    #    latest_checkpoint = False)
                       latest_checkpoint = True)

# %% --------------------------------------------------
experiment = Experiment(exp_dir = args.exp_dir, eval_last_checkpoint=bool(args.latest_checkpoint))
cfg = experiment.cfg
cfg.DATASET.TEST = 'cifar10'

# %% --------------------------------------------------
# Define dataloaders
test_loader_rel, test_loader_ind = prepare_dataloaders(cfg)


# %% --------------------------------------------------
# examine the images obtained from the dataloaders
for (X, y) in test_loader_ind:
    break

images = rearrange(X, "b c w h -> b w h c").detach().numpy()

idx = 0
img_min = images[idx, ...].min()
img_max = images[idx, ...].max()
plt.imshow((images[0, ...] - img_min) / (img_max - img_min))


# %% --------------------------------------------------
# compute mean etalons for every class from the CLS token on the _ind dataset (individual images)
import gc

to_compute = False      # otherwise pre-computed etalons are loaded from a file

if to_compute:
    etalons = torch.zeros((10, 1024))
    counts = torch.zeros((10,))
    experiment.model.eval()
    experiment.model.dinov2.eval()
    with torch.no_grad():
        for (X, y) in test_loader_ind:
            out_dict = experiment.model.dinov2.forward_features(X.to(experiment.model.device))
            for i in range(cfg.INPUT.BATCH_SIZE):
                cls = y[i].item()
                counts[cls] += 1
                etalons[cls, :] = (counts[cls] - 1) / counts[cls] * etalons[cls, :] + 1 / counts[cls] * out_dict['x_norm_clstoken'][i, :].cpu().detach()
            del out_dict
            gc.collect()
            torch.cuda.empty_cache()
    np.savez('cifar10_dinov2_cls_mean_etalons.npz', etalons=etalons.numpy())
else:
    etalons = torch.tensor(np.load('cifar10_dinov2_cls_mean_etalons.npz')['etalons'])


# %% --------------------------------------------------
# collect patch tokens for a batch of cifar10_2x2rel images
for (X, y) in test_loader_rel:
    break


images = rearrange(X, "b c w h -> b w h c").detach().numpy()

idx = 0
img_min = images.min()
img_max = images.max()
images = (images - img_min) / (img_max - img_min)

with torch.no_grad():
    out_dict = experiment.model.dinov2.forward_features(X.to(experiment.model.device))

ptokens = out_dict['x_norm_patchtokens'].cpu()

del out_dict
gc.collect()
torch.cuda.empty_cache()

# %% --------------------------------------------------
import matplotlib.patches as mpatches
cls_names = ['plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']
idx = 3

dist = torch.cdist(ptokens[idx, ...], etalons, p=2)
amin = torch.argmin(dist, dim=1)

# show the input image
plt.figure()
plt.imshow(images[idx])
plt.title('input image')

# show the nearest neighbour classes for each patch
cls_img = amin.reshape((24, 24))
plt.figure()
im = plt.imshow(cls_img, cmap='Set1', interpolation='none')

# cmap -> rgb values
cmap = plt.get_cmap('Set1')
rgb_cm = np.array(cmap.colors)

values = np.unique(amin)
# create a patch (proxy artist) for every color 
patches = [ mpatches.Patch(color=list(rgb_cm[i]), label=f"{cls_names[values[i]]}") for i in range(len(values)) ]
# put those patched as legend-handles into the legend
plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. )

plt.title('DINOv2 patch NN clas')
# %% --------------------------------------------------
