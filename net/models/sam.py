import torch
from torch import nn
from types import SimpleNamespace
from einops import rearrange

from segment_anything import sam_model_registry
from net.common import LayerNorm2d


def remove_padding(orig_size, patch_sz, emb):
    hp = orig_size[0] // patch_sz
    wp = orig_size[1] // patch_sz
    return emb[..., :hp, :wp]


class SAMNet(nn.Module):
    """
    A wrapper around a pre-trained Segment-Anything network (https://github.com/facebookresearch/segment-anything)
    """
    def __init__(self, cfg):
        super(SAMNet, self).__init__()
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.patch_sz = cfg.MODEL.PATCH_SIZE
        assert self.patch_sz == 16, "MODEL.PATCH_SIZE has to be set to 16 for base SAM model!"

        # load sam model 
        self.sam = sam_model_registry[cfg.MODEL.ARCH](checkpoint=cfg.MODEL.PRETRAINED_WEIGHTS).to(self.device)

        # froze parameters
        for p in self.sam.parameters():
            p.requires_grad = False
        self.sam.eval()

    def forward(self, x):
        # Transform input to SAM format:
        #  - values are normalized by mean and std from self.sam.pixel_std + self.sam.pixel_mean
        #  - add padding such that the image is square
        # NOTE: padding needs to be remove to obtain original aspect ratio at some point
        x_transformed = self.sam.preprocess(x)
        with torch.no_grad():
            # [B, C, H, W]
            emb = self.sam.image_encoder(x_transformed)
            # remove padding
            emb = remove_padding(x.shape[-2:], self.patch_sz, emb)
            emb = rearrange(emb, "b c h w -> b h w c")

        return SimpleNamespace(emb = emb)

class SAMNetWONeck(nn.Module):
    """
    A wrapper around a pre-trained Segment-Anything network (https://github.com/facebookresearch/segment-anything)
    """
    def __init__(self, cfg):
        super(SAMNetWONeck, self).__init__()
        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.patch_sz = cfg.MODEL.PATCH_SIZE
        assert self.patch_sz == 16, "MODEL.PATCH_SIZE has to be set to 16 for base SAM model!"

        # load sam model 
        self.sam = sam_model_registry[cfg.MODEL.ARCH](checkpoint=cfg.MODEL.PRETRAINED_WEIGHTS).to(self.device)

        # froze parameters
        for p in self.sam.parameters():
            p.requires_grad = False
        self.sam.eval()


    def forward(self, x):
        x_transformed = self.sam.preprocess(x)
        with torch.no_grad():
            # from SAM VIT transformer forward function, except the neck is not applied tmp after permutation
            tmp = self.sam.image_encoder.patch_embed(x_transformed)
            if self.sam.image_encoder.pos_embed is not None:
                tmp = tmp + self.sam.image_encoder.pos_embed
            for blk in self.sam.image_encoder.blocks:
                tmp = blk(tmp)
            # [B, C, H, W]
            emb = tmp.permute(0, 3, 1, 2)
            # emb = torch.nn.functional.normalize(emb, dim=1)

            # remove padding
            emb = remove_padding(x.shape[-2:], self.patch_sz, emb)
            emb = rearrange(emb, "b c h w -> b h w c")

        return SimpleNamespace(emb = emb)


class SAMNetBaseline(SAMNet):
    def __init__(self, cfg):
        super(SAMNetBaseline, self).__init__(cfg)

        self.decoder = torch.nn.Sequential(
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.GELU(), 
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        # Transform input to SAM format:
        #  - values are normalized by mean and std from self.sam.pixel_std + self.sam.pixel_mean
        #  - add padding such that the image is square
        # NOTE: padding needs to be remove to obtain original aspect ratio at some point
        x_transformed = self.sam.preprocess(x.float())
        with torch.no_grad():
            # [B, C, pH, pW]
            emb = self.sam.image_encoder(x_transformed)
            # remove padding
            emb = remove_padding(x.shape[-2:], self.patch_sz, emb)

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class SAMNetWONeckBaseline(SAMNetWONeck):
    def __init__(self, cfg):
        super(SAMNetWONeckBaseline, self).__init__(cfg)

        self.decoder = torch.nn.Sequential(
                LayerNorm2d(cfg.MODEL.EMB_SIZE),
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.GELU(), 
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        # Transform input to SAM format:
        #  - values are normalized by mean and std from self.sam.pixel_std + self.sam.pixel_mean
        #  - add padding such that the image is square
        # NOTE: padding needs to be remove to obtain original aspect ratio at some point
        x_transformed = self.sam.preprocess(x.float())

        emb = rearrange(SAMNetWONeck.forward(self, x).emb, "b h w c -> b c h w")

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class SAMNetUP(SAMNet):
    def __init__(self, cfg):
        super(SAMNetUP, self).__init__(cfg)
        self.dropout = 0.2

        self.decoder = torch.nn.Sequential(
                nn.Conv2d(cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, 4*cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(4*cfg.MODEL.EMB_SIZE, 4*cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(4*cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        # Transform input to SAM format:
        #  - values are normalized by mean and std from self.sam.pixel_std + self.sam.pixel_mean
        #  - add padding such that the image is square
        # NOTE: padding needs to be remove to obtain original aspect ratio at some point
        x_transformed = self.sam.preprocess(x.float())
        with torch.no_grad():
            # [B, C, pH, pW]
            emb = self.sam.image_encoder(x_transformed)
            # remove padding
            emb = remove_padding(x.shape[-2:], self.patch_sz, emb)

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class SAMNetWONeckUP(SAMNetWONeck):
    def __init__(self, cfg):
        super(SAMNetWONeckUP, self).__init__(cfg)
        self.dropout = 0.2

        self.decoder = torch.nn.Sequential(
                LayerNorm2d(cfg.MODEL.EMB_SIZE),
                nn.Conv2d(cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE//2, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=2.0),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(cfg.MODEL.EMB_SIZE//2, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        x_transformed = self.sam.preprocess(x.float())

        emb = rearrange(SAMNetWONeck.forward(self, x).emb, "b h w c -> b c h w")

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)
