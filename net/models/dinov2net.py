import torch
from torch import nn
from types import SimpleNamespace
from einops import rearrange


class DINOv2Net(nn.Module):
    """
    A wrapper around a pre-trained DINOv2 network (https://github.com/facebookresearch/dinov2)
    """
    def __init__(self, cfg):
        super(DINOv2Net, self).__init__()
        self.device = "cuda" if torch.cuda.is_available() else "cpu"

        self.patch_sz = cfg.MODEL.PATCH_SIZE
        assert self.patch_sz == 14, "MODEL.PATCH_SIZE has to be set to 14 for base DINOv2 model!"

        # load dino model 
        self.dinov2 = torch.hub.load('facebookresearch/dinov2', cfg.MODEL.ARCH).to(self.device)

        # froze parameters
        for p in self.dinov2.parameters():
            p.requires_grad = False
        self.dinov2.eval()

    def forward(self, x):
        with torch.no_grad():
            out_dict = self.dinov2.forward_features(x)
            # emb = rearrange(out_dict["x_norm_patchtokens"], "b (h w) c -> b h w c", h=int(x.shape[2]/self.patch_sz))
            emb = out_dict["x_norm_clstoken"]
            # emb = torch.nn.functional.normalize(emb, dim=-1)
        return SimpleNamespace(logits = None, emb = emb)


class DINOv2NetMultiScale(nn.Module):
    """
    A wrapper around a pre-trained DINOv2 network (https://github.com/facebookresearch/dinov2)
    """
    def __init__(self, cfg):
        super(DINOv2NetMultiScale, self).__init__()
        self.device = "cuda" if torch.cuda.is_available() else "cpu"

        if isinstance(cfg.MODEL.MULTISCALE, int):
            self.blocks_ids =(23 - torch.arange(cfg.MODEL.MULTISCALE)).tolist()
        elif isinstance(cfg.MODEL.MULTISCALE, list):
            self.blocks_ids = cfg.MODEL.MULTISCALE
        else:
            raise TypeError

        self.patch_sz = cfg.MODEL.PATCH_SIZE
        assert self.patch_sz == 14, "MODEL.PATCH_SIZE has to be set to 14 for base DINOv2 model!"

        # load dino model 
        self.dinov2 = torch.hub.load('facebookresearch/dinov2', cfg.MODEL.ARCH).to(self.device)

        # froze parameters
        for p in self.dinov2.parameters():
            p.requires_grad = False
        self.dinov2.eval()

    def forward(self, x):
        with torch.no_grad():
            # list of [B (H W) C]
            out_ms = self.dinov2.get_intermediate_layers(x, n=self.blocks_ids)

        return out_ms




class DINOv2NetBaseline(DINOv2Net):
    def __init__(self, cfg):
        super(DINOv2NetBaseline, self).__init__(cfg)

        self.decoder = torch.nn.Sequential(
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.GELU(), 
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        emb = rearrange(DINOv2Net.forward(self, x).emb, "b h w c -> b c h w")

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class  DINOv2NetUP(DINOv2Net):
    def __init__(self, cfg):
        super(DINOv2NetUP, self).__init__(cfg)
        self.dropout = 0.2

        self.decoder = torch.nn.Sequential(
                nn.Conv2d(cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE//2, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(cfg.MODEL.EMB_SIZE//2, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        emb = rearrange(DINOv2Net.forward(self, x).emb, "b h w c -> b c h w")

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class  DINOv2NetMultiScaleBaseline(DINOv2NetMultiScale):
    def __init__(self, cfg):
        super(DINOv2NetMultiScaleBaseline, self).__init__(cfg)
        
        num_layers = len(self.blocks_ids)
        self.decoder = torch.nn.Sequential(
                nn.Conv2d(num_layers*cfg.MODEL.EMB_SIZE, num_layers*cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.GELU(), 
                nn.Conv2d(num_layers*cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        # list of [B (H W) C]
        emb_list = DINOv2NetMultiScale.forward(self, x)
        emb = rearrange(torch.cat(emb_list, dim=-1), "b (h w) c -> b c h w", h=int(x.shape[2]/self.patch_sz))

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class  DINOv2NetMultiScaleBaselineLP(DINOv2NetMultiScale):
    def __init__(self, cfg):
        super(DINOv2NetMultiScaleBaselineLP, self).__init__(cfg)
        
        num_layers = len(self.blocks_ids)
        self.decoder = torch.nn.Sequential(
                nn.Conv2d(num_layers*cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        # list of [B (H W) C]
        emb_list = DINOv2NetMultiScale.forward(self, x)
        emb = rearrange(torch.cat(emb_list, dim=-1), "b (h w) c -> b c h w", h=int(x.shape[2]/self.patch_sz))

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)


class  DINOv2NetUPMultiScale(DINOv2NetMultiScale):
    def __init__(self, cfg):
        super(DINOv2NetUPMultiScale, self).__init__(cfg)
        self.dropout = 0.2

        self.up = nn.UpsamplingBilinear2d(scale_factor=1.9) 
        self.act = nn.LeakyReLU()
        self.dropout = nn.Dropout2d(self.dropout)
        
        self.convs_list = nn.ModuleList([nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1)] + 
            [nn.Conv2d(2*cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1) for i in range(0, len(self.blocks_ids)-1)])

        self.head = nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)


    def forward(self, x):
        out_ms = DINOv2NetMultiScale.forward(self, x)[::-1]

        e_c = None
        for i in range(0, len(self.blocks_ids)):
            e = rearrange(out_ms[i], "b (h w) c -> b c h w", h=int(x.shape[2]/self.patch_sz))
            if e_c is None:
                e_c = e
            else:
                e_c = torch.cat([e_c, torch.nn.functional.interpolate(e, size=e_c.shape[-2:], mode="bilinear")], dim=1)
            e_c = self.convs_list[i](e_c)
            e_c = self.up(e_c)
            e_c = self.act(e_c)
            e_c = self.dropout(e_c)
        
        # [B, num_classes, xH, xW]
        logits = torch.nn.functional.interpolate(self.head(e_c), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(out_ms[0], "b (h w) c -> b c h w", h=int(x.shape[2]/self.patch_sz))
        return SimpleNamespace(logits = logits, emb = emb)


class  DINOv2NetUPMultiScaleSimple(DINOv2NetMultiScale):
    def __init__(self, cfg):
        super(DINOv2NetUPMultiScaleSimple, self).__init__(cfg)
        self.dropout = 0.2

        self.decoder = torch.nn.Sequential(
                nn.Conv2d(4*cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=1, stride=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, 2*cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(2*cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.EMB_SIZE, kernel_size=3, stride=1, padding=1),
                nn.UpsamplingBilinear2d(scale_factor=1.9),
                nn.LeakyReLU(), 
                nn.Dropout2d(self.dropout),
                nn.Conv2d(cfg.MODEL.EMB_SIZE, cfg.MODEL.NUM_CLASSES, kernel_size=1, stride=1)
            )

    def forward(self, x):
        out_ms = DINOv2NetMultiScale.forward(self, x)
        emb = torch.cat(out_ms, dim=-1)
        emb = rearrange(emb, "b (h w) c -> b c h w", h=int(x.shape[2]/self.patch_sz))

        # [B, xH, xW, num_classes]
        logits = torch.nn.functional.interpolate(self.decoder(emb), size=x.shape[-2:], mode="bilinear")

        logits = rearrange(logits, "b c xh xw -> b xh xw c")
        emb = rearrange(emb, "b c ph pw -> b ph pw c")
        return SimpleNamespace(logits = logits, emb = emb)
